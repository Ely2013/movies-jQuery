/* This file contains an arrayFilm with movies and details */
var arrayMoviesVM = [];

arrayMoviesVM.push({
    id: "f1",
    src: "Images\\WarforthePlanetoftheApes.jpg",
    title: "War for the Planet of the Apes"
});

arrayMoviesVM.push({
    id: "f2",
    src: "Images\\BlackButterfly.jpg",
    title: "Black Butterfly"
});

arrayMoviesVM.push({
    id: "f3",
    src: "Images\\WishUpon.jpg",
    title: "Wish Upon"
});


arrayMoviesVM.push({
    id: "f4",
    src: "Images\\UssIndianapolis.jpg",
    title: "Uss Indianapolis",
});

arrayMoviesVM.push({
    id: "f5",
    src: "Images\\BeforeIFall.jpg",
    title: "Before I Fall"
});

arrayMoviesVM.push({
    id: "f6",
    src: "Images\\CHiPs.jpg",
    title: "CHiPs"
});

arrayMoviesVM.push({
    id: "f7",
    src: "Images\\RoughNight.jpg",
    title: "Rough Night"
});

arrayMoviesVM.push({
    id: "f8",
    src: "Images\\TheAssignment.jpg",
    title: "The Assignment"
});

arrayMoviesVM.push({
    id: "f9",
    src: "Images\\DiaryofaWimpyKidTheLongHaul.jpg",
    title: "Diary of a Wimpy Kid: The Long Haul"
});

arrayMoviesVM.push({
    id: "f10",
    src: "Images\\TheDarkTower.jpg",
    title: "The Dark Tower",
});

arrayMoviesVM.push({
    id: "f11",
    src: "Images\\TheHouse.jpg",
    title: "The House"
});

arrayMoviesVM.push({
    id: "f12",
    src: "Images\\AtomicBlonde.jpg",
    title: "Atomic Blonde"
});

arrayMoviesVM.push({
    id: "f13",
    src: "Images\\Overdrive.jpg",
    title: "Overdrive"
});

arrayMoviesVM.push({
    id: "f14",
    src: "Images\\DespicableMe3.jpg",
    title: "Despicable Me 3"
});

arrayMoviesVM.push({
    id: "f15",
    src: "Images\\Dunkirk.jpg",
    title: "Dunkirk"
});

arrayMoviesVM.push({
    id: "f16",
    src: "Images\\MissSloane.jpg",
    title: "Miss Sloane"
});

arrayMoviesVM.push({
    id: "f17",
    src: "Images\\BabyDriver.jpg",
    title: "Baby Driver"
});

arrayMoviesVM.push({
    id: "f18",
    src: "Images\\Cars3.jpg",
    title: "Cars 3"
});

arrayMoviesVM.push({
    id: "f19",
    src: "Images\\Gifted.jpg",
    title: "Gifted"
});

arrayMoviesVM.push({
    id: "f20",
    src: "Images\\AmericanMade.jpg",
    title: "American Made"
});

arrayMoviesVM.push({
    id: "f21",
    src: "Images\\ValerianandTheCityofaThousandPlanet.jpg",
    title: "Valerian and The City of a Thousand Planets"
});

arrayMoviesVM.push({
    id: "f22",
    src: "Images\\Everything,Everything.jpg",
    title: "Everything, Everything"
});

arrayMoviesVM.push({
    id: "f23",
    src: "Images\\AmityvilleTheAwakening.jpg",
    title: "Amityville: The Awakening"
});

arrayMoviesVM.push({
    id: "f24",
    src: "Images\\EmojimovieExpressYourself.jpg",
    title: "Emojimovie: express Yourself"
});   