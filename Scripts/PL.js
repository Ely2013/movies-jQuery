/* This function allows to recover all details of selected movies */
function showPoster() {
    for (var i = 0; i < arrayMoviesVM.length; i++) {
        var result = "";
        var movieSum = arrayMoviesVM[i];
        result = result + "<div class=\"rcorners-loc\" id=\"box-film-" + movieSum.id + "\">";
        result = result + "<div class=\"flex-item-loc\">";
        result = result + "<article>";
        result = result + "<img class=\"myImg-loc\" id=\"" + movieSum.id + "\" src=\"" + movieSum.src + "\">";
        result = result + "<input type=\"submit\" value=\"Trailer\" onclick=\"clickTrailer(event, '" + movieSum.id + "')\">";
        result = result + "<input type=\"submit\" value=\"Scheda\" onclick=\"clickInfo(event, '" + movieSum.id + "')\">";
        result = result + "</article>";
        result = result + "</div>";
        result = result + "</div>";
        $("#poster-container").append(result);
    }
}

$(document).ready(function () {
    showPoster();
});

/*This function allows,through event and id received, to search the id in the function getTrailer and to return trailer of
selected movies in popup*/
function clickTrailer(event, idMovie) {
    var iframe = "";
    var trailerLink = getTrailer(idMovie);
    var iframe = "<iframe width=1200 height=550 src=\"" + trailerLink + "\" frameborder=0 allowfullscreen></iframe>";
    $("#trailer-content").html(iframe);
    $(".modal-trailer").show();
}

/*This function allows, through event and id received, to search the id in the function getDettagli and to return selected movies*/
function clickInfo(event, id) {
    var movieDetails = getDetails(id);
    showDetails(movieDetails);
}

/*This function allow through event to report on the modal movie's details*/
function showDetails(selectMovie) {
    if (selectMovie) {
        $("#dynamicImage").attr("src", selectMovie.src);
        $("#dynamicTitle").html("<b> " + selectMovie.title + "</b>");
        $("#dynamicKind").html("<b>Kind: </b>" + selectMovie.kind);
        $("#dynamicCountry").html("<b>Country: </b>" + selectMovie.country);
        $("#dynamicYear").html("<b>Year: </b>" + selectMovie.year);
        $("#dynamicRunningTime").html("<b>Running time: </b>" + selectMovie.runningTime);
        $("#dynamicDirectedBy").html("<b>Directed By: </b>" + selectMovie.directedBy);
        $("#dynamicStarring").html("<b>Starring: </b>" + selectMovie.starring);
        $("#dynamicReleaseDate").html("<b>Release Date: </b>" + selectMovie.releaseDate);
        $("#dynamicPlot").html("<b>Plot: </b>" + "<br><br>" + selectMovie.plot + "<br><br>");
        $("#myModal").show();
    }
    else {
        alert("Non esiste il film");
    }
}

// When the user clicks on <span> (x), close the modal
function closeSpan() {
    $("#myModal").hide();
    $("#trailer-modal").hide();
}

/*This function allows to validate/verify the form and returns flexbox of selected movie*/
function validateForm() {
    // arrayMoviesDM.filter(function(film) { return film.title.indexOf(x) > -1; }).map(miomap);
    // showPoster();

    $("#hidden").hide();
    var x = $("#formResearch").val();
    for (var i = 0; i < arrayMoviesVM.length; i++) {
        if (arrayMoviesVM[i].title.indexOf(x) > -1) {
            $("#box-film-" + arrayMoviesVM[i].id).css("display", "block");
        }
        else {
            $("#box-film-" + arrayMoviesVM[i].id).css("display", "none");
        }
    }
    if ($("div[id^='box-film-']:visible").length == 0) {
        $("#hidden").show();
    }
}

/*This function shows only movies's flexbox of the selected month*/
function movieMonth(event) {
    var refMonth = $("#selectMonth").val();
    if (refMonth == "Month") {
        location.reload();
    }
    for (var i = 0; i < arrayMoviesDM.length; i++) {
        var month = arrayMoviesDM[i].releaseDate.slice(3, 6);
        if (refMonth == month) {
            $("#box-film-" + arrayMoviesDM[i].id).show();
        }
        else {
            $("#box-film-" + arrayMoviesDM[i].id).hide();
        }
    }
    $("#period").html(monthActualy(refMonth));
}

/*This function returns current month and it called from the function movieMonth*/
function monthActualy(month) {
    switch (month) {
        case "jul":
            period = "July 2017";
            break;
        case "aug":
            period = "August 2017";
            break;
        case "sep":
            period = "September 2017";
            break;
        case "Month":
            period = "Movies 2017";
    }
    return period;
}

/*This function shows only movies's flexbox of the selected kind*/
function movieKind() {
    var type = $("#selectKind").val();
    if (type == "Kind") {
        location.reload();
    }
    for (var i = 0; i < arrayMoviesDM.length; i++) {
        var kind = arrayMoviesDM[i].kind;
        if (type == kind) {
            $("#box-film-" + arrayMoviesDM[i].id).show();
        }
        else {
            $("#box-film-" + arrayMoviesDM[i].id).hide();
        }
    }
}