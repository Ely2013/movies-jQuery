/* This file contains an arrayFilm with movies and details */
var arrayMoviesDM = [];

arrayMoviesDM.push({
    id: "f1",
    src: "Images\\WarforthePlanetoftheApes.jpg",
    title: "War for the Planet of the Apes",
    kind: "Science fiction",
    country: "Usa",
    year: 2017,
    runningTime: 150,
    directedBy: "Matt Reeves",
    starring: "Woody Harrelson, Judy Gree, Andy Serkis, Steve Zahn, Max Lloyd-Jones, Ty Olsson",
    releaseDate: "13 jul 2017",
    plot: "Two years after the events of Dawn of the Planet of the Apes," +
    "<br>" + "Caesar and his apes have been at war against the humans. As the apes" +
    "<br>" + "start suffering heavy losses, Caesar wrestles with his darker instincts" +
    "<br>" + "in order to face the Colonel and the traitorous gorilla Red. This confrontation will" +
    "<br>" + "determine the fate of apes and humans alike and the future of Earth.",
    trailer: "https://www.youtube.com/embed/JDcAlo8i2y8"
});

arrayMoviesDM.push({
    id: "f2",
    src: "Images\\BlackButterfly.jpg",
    alt: "Black Butterfly",
    title: "Black Butterfly",
    kind: "Thriller",
    country: "Usa, Italia",
    year: 2017,
    runningTime: 90,
    directedBy: "Brian GoodMan",
    starring: "Antonio Banderas, Jonathan Rhys Meyers, Piper Perabo, Abel Ferrara, Nicholas Aaron",
    releaseDate: "13 jul 2017",
    plot: "Paul is a screenwriter who can not write anything since his wife left him four" +
    "<br>" + "years earlier. He is forced to sell his old farmhouse in the woods" +
    "<br>" + "to support himself, but nobody seems interested in buying it." +
    "<br>" + "His creativity puts to the test when he meets on the street Jack, " +
    "<br>" + "a tramp who seems to have a tortous past behind him.",
    trailer: "https://www.youtube.com/embed/MFOaolQR6jk"
});

arrayMoviesDM.push({
    id: "f3",
    src: "Images\\WishUpon.jpg",
    alt: "Wish Upon",
    title: "Wish Upon",
    kind: "Horror",
    country: "Usa",
    year: 2017,
    runningTime: 89,
    directedBy: "John R. Leonetti",
    starring: "Joey King, Ryan Philippe, KI Hong Lee, Shannon Purser, Sydney Parks",
    releaseDate: "13 jul 2017",
    plot: "Twelve years after discovering her mother's suicide, Clare is the" +
    "<br>" + "victim of bullying in a school, she feels embarrassed from the behavior of " +
    "<br>" + "her father Jonathaned because she ignored by the boy she likes. All that" +
    "<br>" + "it changes when his father brings an old music box which promise to futfill" +
    "<br>" + "her seven wishes. Firstly she is skeptic, then she realises that every wish" +
    "<br>" + "happens improving her existence. Soon the closest people" +
    "<br>" + "begin to die in violent ways after every wish.",
    trailer: "https://www.youtube.com/embed/CyYEAq6Z-tk"
});

arrayMoviesDM.push({
    id: "f4",
    src: "Images\\UssIndianapolis.jpg",
    alt: "Uss Indianapolis",
    title: "Uss Indianapolis",
    kind: "War",
    country: "Usa",
    year: 2016,
    runningTime: 130,
    directedBy: "Mario Van Peenbles",
    starring: "Nicholas Cage, Tom Sizemore, Thomas Jane, Matt Lanter, Weronika Rosati, Cordy Walker",
    releaseDate: "19 jul 2017",
    plot: "In 1945, the Portland-class heavy cruiser USS Indianapolis, commanded by " +
    "<br>" + "Captain Charles McVay delivers parts of the atomic bomb that would later be used " +
    "<br>" + "to bomb Hiroshima during the end of World War II. While patrolling in the " +
    "<br>" + "Philippine Sea, on July 30, 1945, the ship was torpedoed and sunk by the Imperial " +
    "<br>" + "Japanese Navy (IJN) submarine I-58, taking 300 crewmen with it to the bottom of " +
    "<br>" + "the Philippine Sea, while the rest climb out of the ship and are left stranded at sea " +
    "<br>" + "for five days without food and water and left in shark-infested waters.",
    trailer: "https://www.youtube.com/embed/ZDPE-NronKk"
});

arrayMoviesDM.push({
    id: "f5",
    src: "Images\\BeforeIFall.jpg",
    alt: "Before I Fall",
    title: "Before I Fall",
    kind: "Dramatic",
    country: "Usa",
    year: 2017,
    runningTime: 99,
    directedBy: "Ry Russo-Young",
    starring: "Zoey Deutch, Halston Sage, Logan Miller, Kian Lawley, Elena Kampouris, Diego Bonita",
    releaseDate: "19 jul 2017",
    plot: "Samantha has everything she could want: a fantastic boyfriend," +
    "<br>" + "incredible friends and beauty. But she will soon have" +
    "<br>" + "to deal with her own death,which he will try to understand discovering" +
    "<br>" + "the true value of all that he might lose",
    trailer: "https://www.youtube.com/embed/f5hIJsdIQCc"
});

arrayMoviesDM.push({
    id: "f6",
    src: "Images\\CHiPs.jpg",
    alt: "CHiPs",
    title: "CHiPs",
    kind: "Action",
    country: "Usa",
    year: 2017,
    runningTime: 100,
    directedBy: "Dax Shepard",
    starring: "Dax Shepard, Michael Pena, Kristen Bell, Rosa Salazar, Adam Brody, Jessica McNamee",
    releaseDate: "20 jul 2017",
    plot: "Jon Baker and Frank Ponch Poncharello have just entered the California Highway " +
    "<br>" + "Patrol in Los Angeles for very different reasons. Baker is a professional" +
    "<br>" + "motorcyclist who is trying to put order in his life and in his marriage." +
    "<br>" + "Poncherello,on the other hand, is a federal undercover agent who is" +
    "<br>" + "examinating a multimillion-dollar robbery.they will form a formidable team" +
    "<br>" + "forced to make a couple inthe street on their bikes",
    trailer: "https://www.youtube.com/embed/SV6S2COhVuM"
});

arrayMoviesDM.push({
    id: "f7",
    src: "Images\\RoughNight.jpg",
    alt: "Rough Night",
    title: "Rough Night",
    kind: "Comedy",
    country: "Usa",
    year: 2017,
    runningTime: 101,
    directedBy: "Lucia Aniello",
    starring: "Scarlett Johansson, Kate McKinnon, Jillian Bell, Ilana Glazer, Zoe Kravitz",
    releaseDate: "27 jul 2017",
    plot: "Five college girlfriends reviewed after ten years for a week-end" +
    "<br>" + "in the wildest of bunny litters. Things will take an unexpected bend" +
    "<br>" + "when they accidentally kill a stripper.",
    trailer: "https://www.youtube.com/embed/vaVR9-rTUTI"
});

arrayMoviesDM.push({
    id: "f8",
    src: "Images\\TheAssignment.jpg",
    alt: "The Assignment",
    title: "The Assignment",
    kind: "Thriller",
    country: "Usa",
    year: 2016,
    runningTime: 95,
    directedBy: "Walter Hill",
    starring: "Michelle Rodriguez, Sigourney Weaver, Tony Shalhoub, Anthony LaPaglia, Caitiln Gerard",
    releaseDate: "27 jul 2017",
    plot: "A terrible assassin ends up in the hands of a surgeon who, per vendetta," +
    "<br>" + "for revenge,submits to a sex change operation, making him a woman. While " +
    "<br>" + "modifying his body,he will not lose the ferocity and the wish to avenge those " +
    "<br>" + "who have inflicted such a ruthless punishment",
    trailer: "https://www.youtube.com/embed/HdRBBDd23bA"
});

arrayMoviesDM.push({
    id: "f9",
    src: "Images\\DiaryofaWimpyKidTheLongHaul.jpg",
    alt: "Diary of a Wimpy Kid: The Long Haul",
    title: "Diary of a Wimpy Kid: The Long Haul",
    kind: "Comedy",
    country: "Usa",
    year: 2017,
    runningTime: 90,
    directedBy: "David Bowers",
    starring: "Alicia Silverstone, Tom Everett Scott, Charlie Wright, Jason Drucker, Owen Asztalos",
    releaseDate: "10 aug 2017",
    plot: "Greg get his family to take part in the celebrations of the 90th birthday" +
    "<br>" + "of grand-grand.mother. In fact, the trip is just a justification" +
    "<br>" + "to be able to attend a conference of nearby players." +
    "<br>" + "Obviously things won't go according to their plans.",
    trailer: "https://www.youtube.com/embed/r7PLDbPPL7w"
});

arrayMoviesDM.push({
    id: "f10",
    src: "Images\\TheDarkTower.jpg",
    alt: "The Dark Tower",
    title: "The Dark Tower",
    kind: "Advenuture",
    country: "Usa",
    year: 2017,
    runningTime: 100,
    directedBy: "Nikolay Acel",
    starring: "Idris Elba, Matthew McConaughey, Tom taylor, Katheryn Winnick, Claudia Kim",
    releaseDate: "10 aug 2017",
    plot: "The gunman Roland Deschain hovers in a landscape similar to Old West in" +
    "<br>" + "search of the Black Tower hoping that by reaching it can preserve its dying world",
    trailer: "https://www.youtube.com/embed/GjwfqXTebIY"
});

arrayMoviesDM.push({
    id: "f11",
    src: "Images\\TheHouse.jpg",
    alt: "The House",
    title: "The House",
    kind: "Comedy",
    country: "Usa",
    year: 2017,
    runningTime: 88,
    directedBy: "Andrew Jay Cohen",
    starring: "Will Ferrell, Amy Poehler, Allison Tolman, Sam Richardson, Micaela Watkins",
    releaseDate: "10 aug 2017",
    plot: "After losing daughter Alex's college funds, Scott and Kate Johansen are" +
    "<br>" + "looking for a new source of income to allow the girl to pursue " +
    "<br>" + "her dream of attending college. With the help of neighbor, Frank they" +
    "<br>" + "then decide to set up an underground  casino in the basement of his home.",
    trailer: "https://www.youtube.com/embed/TUhT8CeveG4"
});

arrayMoviesDM.push({
    id: "f12",
    src: "Images\\AtomicBlonde.jpg",
    alt: "Atomic Blonde",
    title: "Atomic Blonde",
    kind: "Thriller",
    country: "Usa, Gran Bretagna, Germania",
    year: 2017,
    runningTime: 115,
    directedBy: "David Leitch",
    starring: "Charlize Theron, John Goodman, James McAvoy, Sofia Boutella, Tody Jones, Eddie Marsan",
    releaseDate: "17 aug 2017",
    plot: "In 1989, on the eve of the collapse of the Berlin Wall and the shifting of" +
    "<br>" + "superpower alliances, Lorraine Broughton, a top-level spy for MI6, is dispatched " +
    "<br>" + "to Berlin to take down a ruthless espionage ring that has just killed an undercover" +
    "<br>" + "agent for reasons unknown. She is ordered to cooperate with Berlin station " +
    "<br>" + "chief David Percival, and the two form an uneasy alliance,unleashing their" +
    "<br>" + "full arsenal of skills in pursuing a threat that jeopardizes the West’s" +
    "<br>" + "entire intelligence operation",
    trailer: "https://www.youtube.com/embed/yIUube1pSC0"
});

arrayMoviesDM.push({
    id: "f13",
    src: "Images\\Overdrive.jpg",
    alt: "Overdrive",
    title: "Overdrive",
    kind: "Action",
    country: "Francia",
    year: 2017,
    runningTime: 96,
    directedBy: "Antonio Negret",
    starring: "Scott Eastwood, Freddie Thorp, Ana De Armas, Gaia Weiss, Simon Abkarian",
    releaseDate: "23 aug 2017",
    plot: "Andrew and Garrett Foster specialize in stealing vintage cars.Hired to steal" +
    "<br>" + "a magnificent Bugatti of 1937, they make an unprecedented bold blow, as " +
    "<br>" + "masterful as unfortunate: the blow is to the detriment of Morier known mafioso," +
    "<br>" + "rival of Max Klemp, the boss on the rise of the German mafia who chose to" +
    "<br>" + "settle in the Côte d'Azur When Morier endangers their life and that of " +
    "<br>" + "Andrew's girlfriend,two thieves have no choice but to accept the" +
    "<br>" + "criminal's proposal and try to steal the Klemp's 1962 Ferrari 250GT",
    trailer: "https://www.youtube.com/embed/SThdF87lzOA"
});

arrayMoviesDM.push({
    id: "f14",
    src: "Images\\DespicableMe3.jpg",
    alt: "Despicable Me 3",
    title: "Despicable Me 3",
    kind: "Computer Animated",
    country: "Usa",
    year: 2017,
    runningTime: 96,
    directedBy: "Kyle Balda, Pierre Coffin",
    starring: "Steve Carell, Trey Parker, Kristen Wiig, Miranda Cosgrove, Russell Brand, Pierre Coffin",
    releaseDate: "24 aug 2017",
    plot: "Gru is faced with a dilemma about his identity, upset by the arrival" +
    "<br>" + "of Balthazar Bratt, a new fearsome enemy, and the discovery of " +
    "<br>" + "a secret twin. But still a turn, the Minions will come back and help him find" +
    "<br>" + "is way again.",
    trailer: "https://www.youtube.com/embed/6DBi41reeF0"
});

arrayMoviesDM.push({
    id: "f15",
    src: "Images\\Dunkirk.jpg",
    alt: "Dunkirk",
    title: "Dunkirk",
    kind: "Action",
    country: "Usa, Gran Bretagna, Francia",
    year: 2017,
    runningTime: 89,
    directedBy: "Christopher Nolan",
    starring: "Tom Hardy, Mark Rylance, Kenneth Branagh, Cilian Murphy, Harry Styles",
    releaseDate: "31 aug 2017",
    plot: "Allied soldiers from Britain, Belgium, Canada and France are surrounded by" +
    "<br>" + "the German Army on the beaches of Dunkirk and evacuated in Operation " +
    "<br>" + "Dynamo between 26 May and 4 June 1940, during the early stages of the Second World War.",
    trailer: "https://www.youtube.com/embed/F-eMt3SrfFU"
});

arrayMoviesDM.push({
    id: "f16",
    src: "Images\\MissSloane.jpg",
    alt: "Miss Sloane",
    title: "Miss Sloane",
    kind: "Dramatic",
    country: "Italia",
    year: 2016,
    runningTime: 132,
    directedBy: "John Madden",
    starring: "Jessica Chastain, Alison Pill, Jake Lacy, Mark Strong, Gugu Mbatha-Raw",
    releaseDate: "31 aug 2017",
    plot: "Elizabeth Sloane, unscrupulous and highly successful political strategist,is back " +
    "<br>" + "against the ruthless world of lobbyists in Washington while discussing" +
    "<br>" + "arms control. The new bill, which requires stricter controls on weapons" +
    "<br>" + "possession, is gaining ground in the Congress and Sloane manifests his ideas, " +
    "<br>" + "against most of his political opponents. Driven by the wish" +
    "<br>" + "to win at all costs,it will end up threatening your career and the " +
    "<br>" + "people close to you.",
    trailer: "https://www.youtube.com/embed/AMUkfmUu44k"
});

arrayMoviesDM.push({
    id: "f17",
    src: "Images\\BabyDriver.jpg",
    alt: "Baby Driver",
    title: "Baby Driver",
    kind: "Action",
    country: "Usa, Gran Bretagna",
    year: 2017,
    runningTime: 115,
    directedBy: "Edgar Wright",
    starring: "Jamie Foxx, Ansel Elgort, Kevin Spacey, Jon Bernthal, Lily James, Jon Hamm",
    releaseDate: "07 sep 2017",
    plot: "A young and talented pilot believes that he is the best in his field. Forced to" +
    "<br>" + "working for a crime boss, he must to do his best when a damned" +
    "<br>" + "robbery (destined to fail) which will threaten his life, his love and his freedom.",
    trailer: "https://www.youtube.com/embed/D9YZw_X5UzQ"
});

arrayMoviesDM.push({
    id: "f18",
    src: "Images\\Cars3.jpg",
    alt: "Cars 3",
    title: "Cars 3",
    kind: "Computer Animated",
    country: "Usa",
    year: 2017,
    runningTime: 109,
    directedBy: "Brian Fre",
    starring: "Owen Wilson, Larry the Cable Guy, Bonnie Hunt, Armie Hummer, Cristela Alonzo",
    releaseDate: "14 sep 2017",
    plot: "Saetta McQueen is used to the role of young champion." +
    "<br>" + "But time passes and he begins to feel a bit obsolete." +
    "<br>" + "To compete in the increasingly technological world of racing" +
    "<br>" + "therefore,he needs help him. Fortunately, in" +
    "<br>" + "his rescue it will help Hispanic Cruz Ramirez who will train him" +
    "<br>" + "to face its new opponents.",
    trailer: "https://www.youtube.com/embed/dN26X92SOu8"
});

arrayMoviesDM.push({
    id: "f19",
    src: "Images\\Gifted.jpg",
    alt: "Gifted",
    title: "Gifted",
    kind: "Dramatic",
    country: "Usa",
    year: 2017,
    runningTime: 101,
    directedBy: "Marc Webb",
    starring: "Chris Evans, Mckenna Grace, Lindsay duncan, Octavia Spencer, Jenny State",
    releaseDate: "14 sep 2017",
    plot: "Frank Adler is a man who grows by himself Mary niece in" +
    "<br>" + "a coastal city of Florida. Frank to ensure a normal education " +
    "<br>" + "to Mary goes in smoke when the amazing math skills of the small" +
    "<br>" + "discovered by his mother, Evelyn, whose projects are likely to separate" +
    "<br>" + "uncle and grandson forevers.",
    trailer: "https://www.youtube.com/embed/vP0GWpNtdZ8"
});

arrayMoviesDM.push({
    id: "f20",
    src: "Images\\AmericanMade.jpg",
    alt: "American Made",
    title: "American Made",
    kind: "Thriller",
    country: "Usa",
    year: 2017,
    runningTime: 120,
    directedBy: "Doug Liman",
    starring: "Tom Cruise, Domhnall Gleeson, Sarah Wright, Jesse Plemons, Caleb Landry Jones",
    releaseDate: "14 sep 2017",
    plot: "The true story of Barry Seal, an American aviator e narcotracer." +
    "<br>" + "Once charged with money laundering, and drug trafficking" +
    "<br>" + "Seal found an agreement with the US government becoming" +
    "<br>" + "whistleblower under cover of the CIA and helping to work" +
    "<br>" + "to one of the most famous operations in the agency's history:" +
    "<br>" + "frame the lord of the drug Pablo Escobar.",
    trailer: "https://www.youtube.com/embed/HG9lMOUtyXc"
});

arrayMoviesDM.push({
    id: "f21",
    src: "Images\\ValerianandTheCityofaThousandPlanet.jpg",
    alt: "Valerian and The City of a Thousand Planets",
    title: "Valerian and The City of a Thousand Planets",
    kind: "Science fiction",
    country: "Francia",
    year: 2017,
    runningTime: 129,
    directedBy: "Luc Besson",
    starring: "Dane Dehaan, Cara Delevingne, Clive Owen, Rihanna, Ethan Hawke, Rutger Hauer, Kris Wu",
    releaseDate: "21 sep 2017",
    plot: "Valérian and Laureline are special agents of the government of human territories, " +
    "<br>" + "responsible form,maintaining order in the universe. Looking for something more" +
    "<br>" + "than a mere professional bond, Valérian is tormenting her partner with constant " +
    "<br>" + "romantic proposals that, given her fortune with women, always end with a " +
    "<br>" + "refusal.By order of their commander, Valérian and Laureline engage in a mission " +
    "<br>" + "in the beautiful intergalactic city of Alpha,from every corner of the universe. Over " +
    "<br>" + "time,composed of thousands of different species Alpha has been populated by " +
    "<br>" + "over 17 million creatures, which serve the common good talents, technology and " +
    "<br>" + "resources.Unfortunately, everyone shares the same goals and hidden forces " +
    "<br>" + "threaten the existence of humanity.",
    trailer: "https://www.youtube.com/embed/DX6LPS_PcCw"
});

arrayMoviesDM.push({
    id: "f22",
    src: "Images\\Everything,Everything.jpg",
    alt: "Everything, Everything",
    title: "Everything, Everything",
    kind: "Dramatic",
    country: "Usa",
    year: 2017,
    runningTime: 96,
    directedBy: "Stella Meghie",
    starring: "Amandla Stenberg, Nick Robinson, Anika Noni Rose, Ana de la Reguera",
    releaseDate: "21 sep 2017",
    plot: "Maddy is an eighteen-year-old girl. She is clever, curious and unreal, she is forced " +
    "<br>" + "by a disease to live in thehermetically sealed environment of her home." +
    "<br>" + "Maddy would like to experience the joys of the outside world and live her" +
    "<br>" + "first love story but can't leave the walls of the house. Starting to exchange" +
    "<br>" + "messages with Olly, the near boyfriend,will create a deep bond with him that will" +
    "<br>" + "bring them to risk everything to be together.",
    trailer: "https://www.youtube.com/embed/zfKCqYVDNoA"
});

arrayMoviesDM.push({
    id: "f23",
    src: "Images\\AmityvilleTheAwakening.jpg",
    alt: "Amityville: The Awakening",
    title: "Amityville: The Awakening",
    kind: "Horror",
    country: "Usa",
    year: 2017,
    runningTime: 85,
    directedBy: "Franck Khalfoun",
    starring: "Bella Thorne, Cameron Monaghan, Jennifer Jason Ligh, Taylor Spreitler, Kurtwood Smith",
    releaseDate: "28 sep 2017",
    plot: "Belle, the youngest sister and twin brother, moves with the single mother" +
    "<br>" + "Joan in a new home, cheaper than the previous one and able to save them" +
    "<br>" + "money for the brother's expensive care.When strange phenomena are beginning " +
    "<br>" + "to occur,in the house and the brother heals miraculously, Belle begins to believe " +
    "<br>" + "that his mother is hiding something and it soon becomes clear that it is over in " +
    "<br>" + "the infamous home theater of a ruthless crime time before",
    trailer: "https://www.youtube.com/embed/pCN3Ouo-Hys"
});

arrayMoviesDM.push({
    id: "f24",
    src: "Images\\EmojimovieExpressYourself.jpg",
    alt: "Emojimovie: express Yourself",
    title: "Emojimovie: express Yourself",
    kind: "Computer Animated",
    country: "Usa",
    year: 2017,
    runningTime: 100,
    originTitle: "Emojimoviw: express Yourself",
    directedBy: "Anthony Leondis",
    starring: "T. J. Miller, James Corden, Patrick Stewart, Christina Aguilera, Anna Faris",
    releaseDate: "28 sep 2017",
    plot: "Behind all messaging applications lies Messaggiopolis, a lively" +
    "<br>" + "cities where emoticons live, hoping to be selected from" +
    "<br>" + "owner of the smartphone. Every emo has a single facial expression" +
    "<br>" + "but this does not apply to Gene, an exuberant gas bottle born without filters " +
    "<br>" + "and equipped with multiple expressions. Determined to become normal to be like" +
    "<br>" + "in other emoticons, Gene asks for help to the best friend Gimme-5 and" +
    "<br>" + "infamous Rebel hacker. The three will embark on an adventurous journey" +
    "<br>" + "between the app to find the code that will solve the Gene problem." +
    "<br>" + "Whenever a major danger threatens all the smartphone," +
    "<br>" + "Will has the fate of all the emoticons and their universe in their hands.",
    trailer: "https://www.youtube.com/embed/4yZERI42eRU"
});   