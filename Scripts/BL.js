/* This function accesses to arrayDm and compare to id received and return my movie with details*/
function getDetails(idMovie) {
    for (var i = 0; i < arrayMoviesDM.length; i++) {
        if (idMovie == arrayMoviesDM[i].id) {
            return arrayMoviesDM[i];
        }
    }
}

/* This function accesses to arrayDm and compare to id received and return trailer's movie*/
function getTrailer(idMovieTrailer) {
    for (var i = 0; i < arrayMoviesDM.length; i++) {
        if (idMovieTrailer == arrayMoviesDM[i].id) {
            return arrayMoviesDM[i].trailer;
        }
    }
}